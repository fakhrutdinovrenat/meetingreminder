﻿using System.Collections.Generic;
using System.IO;
using MeetingReminder.FileUtil;
using MeetingReminder.Res;

namespace MeetingReminder
{
    public class FileWriterImpl : FileWriter
    {
        private static FileWriterImpl readerInstance;

        private FileWriterImpl() { }

        public static FileWriterImpl getInstance()
        {
            if (readerInstance == null)
                readerInstance = new FileWriterImpl();

            return readerInstance;
        }

        public void writeMeetings(List<MeetingModel> meetings)
        {
            StreamWriter writer = new StreamWriter(Strings.textFilePath);
            foreach(MeetingModel meeting in meetings)
            {
                writer.WriteLine($"Встреча {meeting.name} {meeting.durationInMinutes} минут с {meeting.startTime}");
            }
            writer.Close();
        }
    }
}
