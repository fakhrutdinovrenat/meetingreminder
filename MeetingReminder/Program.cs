﻿using System;
using MeetingReminder.Data;
using MeetingReminder.FileUtil;
using MeetingReminder.Res;

namespace MeetingReminder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Strings.WelcomeMessage);

            MeetingsRepository repository = MeetingsRepositoryImpl.getInstance();

            showMeetingsInfo(repository);

            FileWriter writer = FileWriterImpl.getInstance();

            Console.WriteLine(Strings.PlanMeeting);

            String inputLine = Console.ReadLine();

            while(!inputLine.Equals("") && !inputLine.ToLower().Equals(Strings.Exit))
            {
                if(inputLine.StartsWith(Strings.Info))
                {
                    var words = splitString(inputLine, 2);
                    if(words.Length == 1)
                    {
                        showMeetingsInfo(repository);
                    } else
                    {
                        DateTime date = DateTime.Parse(words[1]);
                        showMeetingsInfo(repository, date);
                    }
                } else if(inputLine.StartsWith(Strings.Save))
                {
                    var words = splitString(inputLine, 2);
                    if (words.Length == 1)
                    {
                        writer.writeMeetings(repository.getMeetings());
                    } else
                    {
                        writer.writeMeetings(repository.getMeetings(DateTime.Parse(words[1])));
                    }
                } else if(inputLine.StartsWith(Strings.Delete))
                {
                    var words = splitString(inputLine, 2);
                    var deletingKey = words[1];
                    repository.deleteMeeting(deletingKey);
                }

                else
                {
                    var words = splitString(inputLine, 3);

                    try
                    {
                        DateTime date = DateTime.Parse(words[1]);
                        if (date < DateTime.Now) throw new PastMeetingException();

                        // По умолчанию длительность встречи 15 минут.
                        int durationInMinutes = 15;
                        if (words.Length >= 3) durationInMinutes = Int16.Parse(words[2]);
                        repository.saveMeeting(new MeetingModel(name: words[0], startTime: date, durationInMinutes: durationInMinutes));
                    } catch (PastMeetingException)
                    {
                        Console.WriteLine(Strings.PastExceptionMessage);
                    } catch (MeetingIntersectionException)
                    {
                        Console.WriteLine(Strings.IntersectionException);
                    } catch (SameNameException)
                    {
                        Console.WriteLine(Strings.SameNameMessage);
                    }
                    catch
                    {
                        Console.WriteLine(Strings.IncorrectInputMessage);
                    }
                }

                inputLine = Console.ReadLine();
            }
        }

        private static void showMeetingsInfo(MeetingsRepository repository)
        {
            int numberOfMeetings = repository.numberOfMeetings();

            Console.WriteLine($"Запланировано {numberOfMeetings} встреч.");

            if (numberOfMeetings > 0)
            {
                foreach (MeetingModel meeting in repository.getMeetings())
                {
                    Console.WriteLine($"{meeting.name} {meeting.durationInMinutes} минут в {meeting.startTime}");
                }
            }
        }

        private static void showMeetingsInfo(MeetingsRepository repository, DateTime date)
        {
            int numberOfMeetings = repository.numberOfMeetings(date);

            Console.WriteLine($"Запланировано {numberOfMeetings} встреч.");

            if (numberOfMeetings > 0)
            {
                foreach (MeetingModel meeting in repository.getMeetings())
                {
                    Console.WriteLine($"{meeting.name} {meeting.durationInMinutes} минут в {meeting.startTime}");
                }
            }
        }

        private static String[] splitString(String text, int count)
        {
            return text.Split(' ', count);
        }
    }
}
