﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeetingReminder.Data;

namespace MeetingReminder
{
    public class MeetingsRepositoryImpl : MeetingsRepository
    {

        private static MeetingsRepositoryImpl meetingsHolderInstance;

        private Dictionary<String, MeetingModel> dictionary = new Dictionary<String, MeetingModel>();

        private MeetingsRepositoryImpl() {}

        public static MeetingsRepositoryImpl getInstance()
        {
            if (meetingsHolderInstance == null)
                meetingsHolderInstance = new MeetingsRepositoryImpl();

            return meetingsHolderInstance;
        }

        public int numberOfMeetings()
        {
            int number = getMeetings().Count;
            return number;
        }

        public int numberOfMeetings(DateTime date)
        {
            int number = getMeetings(date).Count;
            return number;
        }

        public void saveMeeting(MeetingModel meeting)
        {
            if (dictionary.ContainsKey(meeting.name))
            {
                throw new SameNameException();
            }
            if (!checkIntersectingMeetings(meeting.startTime, meeting.durationInMinutes))
            {
                throw new MeetingIntersectionException();
            }
            
            dictionary.Add(meeting.name, meeting);
        }

        public void deleteMeeting(String key)
        {

            dictionary.Remove(key);
        }

        public List<MeetingModel> getFutureMeetings()
        {
            return dictionary.Values.Where(meeting => meeting.startTime > DateTime.Now).ToList();
        }

        public List<MeetingModel> getMeetings()
        {
            return dictionary.Values.ToList();
        }

        public List<MeetingModel> getMeetings(DateTime date)
        {
            return dictionary.Values.Where(meeting => meeting.startTime.Date == date.Date).ToList();
        }

        private bool checkIntersectingMeetings(DateTime startTime, int duration)
        {
            DateTime endTime = startTime.AddMinutes(duration);
            var futureMeetings = dictionary.Values.Where(meeting => meeting.startTime > DateTime.Now).ToList();
            foreach (MeetingModel meeting in futureMeetings)
            {
                DateTime meetingEndTime = meeting.startTime.AddMinutes(meeting.durationInMinutes);
                if (startTime >= meetingEndTime || endTime <= meeting.startTime) continue;
                return false;
            }
            return true;
        }
    }
}
