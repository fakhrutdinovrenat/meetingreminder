﻿using System;
using System.Collections.Generic;

namespace MeetingReminder.Data
{
    /// <summary>
    /// Хранилище данных о запланированных встречах.
    /// </summary>
    public interface MeetingsRepository
    {
        /// <summary>
        /// Получение общего количества встреч.
        /// </summary>
        /// <returns></returns>
        int numberOfMeetings();


        /// <summary>
        /// Получение количество встречь за определенных день.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        int numberOfMeetings(DateTime date);

        /// <summary>
        /// Получение списка всех будущих встреч.
        /// </summary>
        /// <returns></returns>
        List<MeetingModel> getFutureMeetings();

        /// <summary>
        /// Получение списка всех запланированных встреч.
        /// </summary>
        /// <returns></returns>
        List<MeetingModel> getMeetings();

        /// <summary>
        /// Получение списка всех запланированных встреч за определённый день.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        List<MeetingModel> getMeetings(DateTime date);

        /// <summary>
        /// Сохранение встречи.
        /// </summary>
        void saveMeeting(MeetingModel meeting);

        /// <summary>
        /// Удаление встречи по ключу.
        /// </summary>
        void deleteMeeting(String key);
    }
}
