﻿using System;
namespace MeetingReminder
{
    public struct MeetingModel
    {
        public string name;
        public DateTime startTime;
        public int durationInMinutes;


        public MeetingModel(string name, DateTime startTime, int durationInMinutes = 15)
        {
            this.name = name;
            this.startTime = startTime;
            this.durationInMinutes = durationInMinutes;
        }
    }
}
